import { Component, NgModule, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateModule } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import * as moment from 'moment';

import * as CanvasJS from '../../assets/canvasjs.min';
import { AngularDateTimePickerModule } from 'angular2-datetimepicker';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';

export const MY_FORMATS = {
  parse: {
    dateInput: 'YYYY-MM',
  },
  display: {
    dateInput: 'YYYY-MM-DD',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};



@Component({
  selector: 'app-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.sass'],

  providers: [

    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class GraphComponent implements OnInit {


  chartPM25
  chartTemp


  listPM25 = [];
  listTemp = [];



  csv = []

  listCSV = [];

  uid
  constructor(public http: HttpClient, public route: ActivatedRoute) {






  }


  dt = new FormControl('')
  dtStart = new FormControl('');
  dtEnd = new FormControl('');

  tStart = new FormControl('');
  tEnd = new FormControl('');


  setUp(){





    this.chartPM25 = new CanvasJS.Chart("chartContainerPM25", {
      animationEnabled: true,
      exportEnabled: true,
      zoomEnabled: true,

      colorSet: "greenShades",

      title: {
        text: "กราฟ PM25"
      },

      legend: {
        cursor: "pointer",
        verticalAlign: "top",
        horizontalAlign: "center",
        dockInsidePlotArea: true,
        itemclick: (e) => {

          if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
          } else {
            e.dataSeries.visible = true;
          }
          this.chartPM25.render();

        }
      },

      data: [

        {
      
          type: "column",
          name: "PM2.5",
          showInLegend: true,
          markerSize: 10,
          yValueFormatString: "# ug/m3",
          dataPoints: this.listPM25
        },

      ]
    });

    this.chartPM25.render();


    this.chartTemp = new CanvasJS.Chart("chartContainerTemp", {
      animationEnabled: true,
      exportEnabled: true,
      zoomEnabled: true,
      title: {
        text: "กราฟ Temp"
      },

      legend: {
        cursor: "pointer",
        verticalAlign: "top",
        horizontalAlign: "center",
        dockInsidePlotArea: true,
        itemclick: (e) => {

          if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
          } else {
            e.dataSeries.visible = true;
          }
          this.chartTemp.render();

        }
      },

      data: [

        {
          color: "red",
          type: "column",
          name: "TEMP",
          showInLegend: true,
          markerSize: 10,
          yValueFormatString: "# C",
          dataPoints: this.listTemp
        },

      ]
    });

    this.chartTemp.render();


    



  }
  ngOnInit(): void {

    // this.setUp();


  }

  dateSelect() {

  this.listPM25 = [];
  this.listTemp = [];
  this.listCSV = [];



  




    console.log(moment(this.dt.value).format('YYYY-MM-DD'))

    

    this.http.get("http://35.240.153.210:8989/log/day/test/aqis/" + moment(this.dt.value).format('YYYY-MM-DD') , {})

      .subscribe(

        data => {

          console.log(JSON.stringify(data))

                  

          if (data["status"] == 200) {


   

            var dataColor = [];

        
            for (var i = 0; i < Object.keys(data["message"]).length; i++) {


              var tmp = {};
              tmp["pm25"] = data["message"][i]["pm25"];
              tmp["temp"] = data["message"][i]["temp"];
              tmp["date-time"] = data["message"][i]["dtf"];
  
              this.listCSV.push(tmp);



            if(data["message"][i]["pm25"] >= 0 &&  data["message"][i]["pm25"] <= 25){

              dataColor.push("#29A3CC")
            }else if(data["message"][i]["pm25"] >= 26 &&  data["message"][i]["pm25"] <= 50){
              dataColor.push("#13D44A")
            }else if(data["message"][i]["pm25"] >= 51 &&  data["message"][i]["pm25"] <= 100){
              dataColor.push("#F5EA11")
            }else if(data["message"][i]["pm25"] >= 101 &&  data["message"][i]["pm25"] <= 200){
              dataColor.push("#F7A520")
            }else{
              dataColor.push("#F72032")
            }

              var rawPM25 = {};
              var rawPm10 = {};
     

              rawPM25["label"] = data["message"][i]["dtf"]
              rawPM25["y"] = data["message"][i]["pm25"];
              this.listPM25.push(rawPM25);

              rawPm10["label"] = data["message"][i]["dtf"]
              rawPm10["y"] = data["message"][i]["temp"];
              this.listTemp.push(rawPm10);

              



            }


            console.log(this.listTemp);

            this.csv = this.listCSV;




            CanvasJS.addColorSet("greenShades",
           dataColor
            
            );

            this.setUp();


            this.chartPM25.data[0] = this.listPM25;
            this.chartTemp.data[0] = this.listTemp;


            this.chartPM25.render();
            this.chartTemp.render();

          }
        }

      )



  }



  export(){

    this.downloadFile(this.csv);

  }
  downloadFile(data, filename = 'data') {
    let csvData = this.ConvertToCSV(data, ['pm25', 'temp' , 'date-time']);
    console.log(csvData)
    let blob = new Blob(['\ufeff' + csvData], { type: 'text/csv;charset=utf-8;' });
    let dwldLink = document.createElement("a");
    let url = URL.createObjectURL(blob);
    let isSafariBrowser = navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1;
    if (isSafariBrowser) {  //if Safari open in new window to save file with random filename.
      dwldLink.setAttribute("target", "_blank");
    }
    dwldLink.setAttribute("href", url);
    dwldLink.setAttribute("download", filename + ".csv");
    dwldLink.style.visibility = "hidden";
    document.body.appendChild(dwldLink);
    dwldLink.click();
    document.body.removeChild(dwldLink);
  }

  ConvertToCSV(objArray, headerList) {
    let array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
    let str = '';
    let row = 'S.No,';

    for (let index in headerList) {
      row += headerList[index] + ',';
    }
    row = row.slice(0, -1);
    str += row + '\r\n';
    for (let i = 0; i < array.length; i++) {
      let line = (i + 1) + '';
      for (let index in headerList) {
        let head = headerList[index];

        line += ',' + array[i][head];
      }
      str += line + '\r\n';
    }
    return str;
  }


}
