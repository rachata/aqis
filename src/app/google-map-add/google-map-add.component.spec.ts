import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoogleMapAddComponent } from './google-map-add.component';

describe('GoogleMapAddComponent', () => {
  let component: GoogleMapAddComponent;
  let fixture: ComponentFixture<GoogleMapAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoogleMapAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoogleMapAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
