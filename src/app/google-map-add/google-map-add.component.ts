import { Component, OnInit } from '@angular/core';
import { MatSelectModule } from '@angular/material/select';
import { LatLngLiteral, MouseEvent } from '@agm/core';
import { calcPossibleSecurityContexts } from '@angular/compiler/src/template_parser/binding_parser';
import { HttpClient } from '@angular/common/http';
import { FormControl } from '@angular/forms';
interface marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
  url: string;
}

@Component({
  selector: 'app-google-map-add',
  templateUrl: './google-map-add.component.html',
  styleUrls: ['./google-map-add.component.sass']
})
export class GoogleMapAddComponent implements OnInit {


  uidText = new FormControl("")
  name = new FormControl("")
  line = new FormControl("")
  position = new FormControl("");

  uid = [];

  urlPlace = "/assets/device.png";
  urlArea = "/assets/area.png";

  tempMark = [];
  markers: marker[] = [];
  areaPoly: LatLngLiteral[] = [

    { "lat": 7.20256138084838, "lng": 100.59174986556172 },
    { "lat": 7.200989843716989, "lng": 100.5959424914322 },
    { "lat": 7.203843647963988, "lng": 100.5970527583263 }
  ]

  constructor(private http: HttpClient) {
    // this.callAPIPin();

    this.areaPoly = [];
  }

  save() {
    // console.log(this.uidText.value);
    // console.log(this.name.value);
    // console.log(this.line.value);
    // console.log(this.position.value);

    // console.log(this.markers[0]["lat"]);
    // console.log(this.markers[0]["lng"]);

    var listArae = [];

    for (var i = 0; i < this.tempMark.length; i++) {

      var raw = {};
      raw["lat"] = this.tempMark[i]["lat"];
      raw["lng"] = this.tempMark[i]["lng"];

      listArae.push(raw);

    }

    var data = {
      "name": this.name.value,
      "lat": this.markers[0]["lat"],
      "lng": this.markers[0]["lng"],
      "uid": this.uidText.value,
      "line": this.line.value,
      "area": listArae
    }


    this.http.post("http://35.240.153.210:8989/machine", data)
      .subscribe(data => {


      })


  }


  zoom: number = 16;
  lat: number = 7.206616148263211
  lng: number = 100.59870491526044

  ngOnInit() {

    this.addPlace();
    this.getUID();

    this.position.setValue(this.lat + "," + this.lng)

  }

  getUID() {
    this.http.get("http://35.240.153.210:8989/uid/empty", {})
      .subscribe(

        data => {

          if (data["status"] == 200) {

            for (var i = 0; i < Object.keys(data["message"]).length; i++) {

              var raw = {};
              raw["uid"] = data["message"][i]["text"]
              raw["note"] = data["message"][i]["note"]


              this.uid.push(raw)
            }
          }
        })
  }

  clickedMarker(label: string, index: number) {
    console.log(`clicked the marker: ${label || index}`);
  }
  mapClicked($event: MouseEvent) {
    this.markers.push({
      lat: $event.coords.lat,
      lng: $event.coords.lng,
      draggable: true,
      url: this.urlArea
    });

    this.tempMark.push({
      lat: $event.coords.lat,
      lng: $event.coords.lng,
      draggable: true
    })
  }
  markerDragEnd(m: marker, $event: MouseEvent) {

    var result = this.tempMark.findIndex(({ lat }) => lat === m.lat);


    if (result == -1) {
      var result = this.markers.findIndex(({ lat }) => lat === m.lat);
      console.log("Rsault - 1");
      console.log(result);

      if (result == 0) {
        var tm: marker = {
          lat: $event["coords"].lat,
          lng: $event["coords"].lng,
          draggable: true,
          url: this.urlPlace
        }
        this.position.setValue($event["coords"].lat + "," + $event["coords"].lng)

        this.markers[result] = tm;

        return;
      }


    }
    if (result != undefined) {

      console.log(result);
      var tm: marker = {
        lat: $event["coords"].lat,
        lng: $event["coords"].lng,
        draggable: true,
        url: this.urlArea
      }
      var area = {
        lat: $event["coords"].lat,
        lng: $event["coords"].lng,
        draggable: true,
        url: this.urlArea
      }

      this.tempMark[result] = area;
    }

  }

  area() {

    this.areaPoly = []
    this.areaPoly = this.tempMark;

  }

  delete() {

    var tempDelete = this.tempMark;

    this.areaPoly = []

    this.tempMark = []
    this.markers = []

    this.markers.push({
      lat: this.lat,
      lng: this.lng,
      draggable: true,
      url: this.urlPlace
    });

  }

  add() {


    console.log(JSON.stringify(this.markers[0]));
    console.log(JSON.stringify(this.tempMark));
  }

  addPlace() {
    this.markers.push({
      lat: this.lat,
      lng: this.lng,
      draggable: true,
      url: this.urlPlace
    });

  }
}



