import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoogleMapPolyComponent } from './google-map-poly.component';

describe('GoogleMapPolyComponent', () => {
  let component: GoogleMapPolyComponent;
  let fixture: ComponentFixture<GoogleMapPolyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoogleMapPolyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoogleMapPolyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
