import { Component, OnInit } from '@angular/core';
import { MouseEvent } from '@agm/core';
import { calcPossibleSecurityContexts } from '@angular/compiler/src/template_parser/binding_parser';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
interface marker {
  lat: number;
  lng: number;
  aqi : number;
  uid : string;
  label?: string;
  draggable: boolean;

}

@Component({
  selector: 'app-google-map',
  templateUrl: './google-map.component.html',
  styleUrls: ['./google-map.component.sass']
})
export class GoogleMapComponent implements OnInit {


  constructor(public router : Router , private http : HttpClient){
    this.callAPIPin();
  }
  ngOnInit(){

  }

 zoom: number = 16;

 lat: number = 7.202751
 lng: number =  100.591946

 clickedMarker(label: string, index: number) {
   console.log(`clicked the marker: ${label || index}`);
 }

 
 markerDragEnd(m: marker, $event: MouseEvent) {
   console.log('dragEnd', m, $event);
 }
 markers: marker[] = [

 ];


 select(m){
    
  this.router.navigateByUrl('/realtime/'+m["uid"]+"/"+m["aqi"]+"/"+m["label"]);

}

 callAPIPin(){


  this.http.get("http://35.240.153.210:8989/machine", {})

  .subscribe(

    data => {
      var json = JSON.parse(JSON.stringify(data))

      console.log(JSON.stringify(json));

      for(var i  = 0; i <Object.keys(json["message"]["data"]).length; i++){


        var markerRaw : marker =   
      
        {
          lat: json["message"]["data"][i]["lat"],
          lng: json["message"]["data"][i]["lng"],
          label: json["message"]["data"][i]["name"],
          aqi : json["message"]["data"][i]["aqi"],
          uid : json["message"]["data"][i]["uid"],
          draggable: false
        }

        this.markers.push(markerRaw);


      }

      
   
      console.log(JSON.stringify(json))
    },

    error => {
      console.log("error")
    }
  );
}

}


