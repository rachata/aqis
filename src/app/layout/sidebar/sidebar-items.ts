import { RouteInfo } from './sidebar.metadata';
export const ROUTES: RouteInfo[] = [
  {
    path: '',
    title: 'Home',
    icon: 'fas fa-home',
    class: 'menu-toggle',
    groupTitle: false,
    submenu: [

      {
        path: '/dashboard/main',
        title: 'ข้อมูล',
        icon: '',
        class: 'ml-menu',
        groupTitle: false,
        submenu: []
      },

      {
        path: '/graph/1',
        title: 'ข้อมูลย้อนหลัง',
        icon: '',
        class: 'ml-menu',
        groupTitle: false,
        submenu: []
      },

     
    ]
  },

];
